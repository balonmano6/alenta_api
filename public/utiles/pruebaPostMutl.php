<?php

$string = file_get_contents("/var/www/library_api/public/utiles/books.json");
$json_a = json_decode($string, true);

//API URL
$url = 'http://api.agual.es/book/addmult';

//create a new cURL resource
$ch = curl_init($url);

//setup request to send json via POST
$data = array(
      "isbn" => "9781593275846",
      "title" => "Eloquent JavaScript, Second Edition",
      "subtitle" => "A Modern Introduction to Programming",
      "author" => "Marijn Haverbeke",
      "published" => "2014-12-14T00:00:00.000Z",
      "publisher" => "No Starch Press",
      "pages" => 472,
      "description" => "JavaScript lies at the heart of almost every modern web application, from social apps to the newest browser-based games. Though simple for beginners to pick up and play with, JavaScript is a flexible, complex language that you can use to build full-scale applications.",
      "website" => "http://eloquentjavascript.net/",
      "category" => "Classic"
);
$payload = json_encode($json_a);

//attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

//set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute the POST request
$result = curl_exec($ch);

//close cURL resource
curl_close($ch);

var_dump($result);
?>
