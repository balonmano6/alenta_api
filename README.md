API PRUEBAS:

Pequeña api útil para una librería, ya que nos permite almacenar, borrar y consultar libros de esta. 


MÉTODOS ADICIÓN LIBROS:

Se han creado un par de métodos para añadir libros.

1. Adición de un libro indivicual. 

	Medíante un post a /book/add pasandole una estructura json como la que mostramos mas a bajo, se puede añadir un libro a la BBDD. 
	Si todo ha ido bien nos devuelve un json con un parametro status = Book Created!, en el caso de que el ISBN del nuevo libro ya estubiera metido nos devuelve un json con el
	parametro status = Book Exist!.

	Ejemplo estructura json:

		{
	      "isbn": "9781593275846",
	      "title": "Eloquent JavaScript, Second Edition",
	      "subtitle": "A Modern Introduction to Programming",
	      "author": "Marijn Haverbeke",
	      "published": "2014-12-14T00:00:00.000Z",
	      "publisher": "No Starch Press",
	      "pages": 472,
	      "description": "JavaScript lies at the heart of almost every modern web application, from social apps to the newest browser-based games. Though simple for beginners to pick up and play with, JavaScript is a flexible, complex language that you can use to build full-scale applications.",
	      "website": "http://eloquentjavascript.net/",
	      "category": "Classic"
    	}

    Ejemplo url funcional: 

    	http://api.agual.es/book/add

2. Adición de una colección de libros.

	Medíante un post a /book/addmult pasandole una estructura json que a su vez contiene un array de libros como la que mostramos mas a bajo, se puede añadir varios libros
	a la BBDD.
	Si todo ha ido bien nos devuelve un json con un parametro status = N Book Created!, donde N es el numbero de libros añadidos. En el caso de que el ISBN de uno de los nuevos
	libros ya estubiera metido salta la adición de este y continua con el siguiente.

	Ejemplo estructura json:

		{
		  "books": [
		    {
		      "isbn": "9781593275846",
		      "title": "Eloquent JavaScript, Second Edition",
		      "subtitle": "A Modern Introduction to Programming",
		      "author": "Marijn Haverbeke",
		      "published": "2014-12-14T00:00:00.000Z",
		      "publisher": "No Starch Press",
		      "pages": 472,
		      "description": "JavaScript lies at the heart of almost every modern web application, from social apps to the newest browser-based games. Though simple for beginners to pick up and play with, JavaScript is a flexible, complex language that you can use to build full-scale applications.",
		      "website": "http://eloquentjavascript.net/",
		      "category": "Classic"
		    },
		    {
		      "isbn": "9781449331818",
		      "title": "Learning JavaScript Design Patterns",
		      "subtitle": "A JavaScript and jQuery Developer's Guide",
		      "author": "Addy Osmani",
		      "published": "2012-07-01T00:00:00.000Z",
		      "publisher": "O'Reilly Media",
		      "pages": 254,
		      "description": "With Learning JavaScript Design Patterns, you'll learn how to write beautiful, structured, and maintainable JavaScript by applying classical and modern design patterns to the language. If you want to keep your code efficient, more manageable, and up-to-date with the latest best practices, this book is for you.",
		      "website": "http://www.addyosmani.com/resources/essentialjsdesignpatterns/book/",
		      "category": "Fantasy"
		    },
		    ...
		  ]
		}

	Ejemplo url funcional: 

		http://api.agual.es/book/addmult

MÉTODOS CONSULTA LIBROS:

Hemos implementado varios métodos GET de consulta. 

1. Todos los libros: Si llamamos a /book sin pasar ningún parametro se nos devolvera un json con todos los libros de la libreria, con todos sus parámetros. 

Ejemplo url funcional: 

	http://api.agual.es/book

2. Libro concreto por ISBN: Si pasamos /book/{ISBN}, donde ISBN es el número concreto de Isbn de un libro, nos devolverá un json con todos los parámetros de ese libro.

Ejemplo url funcional:

	http://api.agual.es/book/9781449331818

3. Todos los libros de una categoría dada: Se llamamos a /book/category/{CATEGORY}, donde CATEGORY es una de las tematicas de los libros, nos devolverá todos los libros y sus
parametros que pertenecen a dicha categoria. 

Ejemplo url funcional:

	http://api.agual.es/book/category/Drama

4. Todos los libros publicados antes de un año dado: Si llamamos a /book/publishedbefore/{YEAR}, donde YEAR es un año en formato YYYY, se nos devolverá todos los libros
publicados antes de esa año, sin incluirlo. 

Ejemplo url funcional:

	http://api.agual.es/book/publishedbefore/2013


MÉTODO ELIMINACIÓN LIBROS

Se ha crado un método DELETE para poder borrar libros.

1. Eliminación de un libro por ISBN: Si realizamos un DELETE en /book/delete/{ISBN}, donde ISBN es el Isbn de un libro, esto nos permitirá borrar dicho libro. 
Si todo ha ido bien se nos devolverá un json con el parámetro status = Book deleted, en caso de que el Isbn no exita se nos devolverá un json con el parametro 
status = Book Not Exist!

Ejemplo url funcional:

	http://api.agual.es/book/delete/9781593275846


UTILES:

Se ha creado una carpeta en /public/utiles donde hay varios php que nos permiten probar los métodos POST de adición de libros y el método DELETE de eliminación.

1. pruebaPost.php: realiza un post de un libro.
2. pruebaPostMult.php: lee el fichero books.json de este mismo directorio y realiza un post con el json leido, para poder añadir varios libros. 
3. pruebaDelete.php: realiza un delete de un libro de la BBDD. 
