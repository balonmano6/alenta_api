<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Repository\BookRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;

class BookController extends AbstractController
{
    private $bookRepository;

    public function __construct(BookRepository $BookRepository)
    {
        $this->bookRepository = $BookRepository;
    }

    /**
     * @Route("/book/add", name="add_book", methods={"POST"})
     */
    public function add(Request $request): JsonResponse
    {
        $data = json_decode($request->getContent(), true);

        $isbn = $data['isbn'];
        $title = $data['title'];
        $subtitle = $data['subtitle'];
        $author = $data['author'];
        $published = date_create($data['published']);
        $publisher = $data['publisher'];
        $pages = $data['pages'];
        $description = $data['description'];
        $website = $data['website'];
        $category = $data['category'];

        if (isset($data['imageUrl'])) {
           $imageUrl = $data['imageUrl']; 
        }
        else{
            $imageUrl = null;
        }
        
        if (empty($isbn) || empty($title) || empty($author) || empty($pages)) {
            throw new NotFoundHttpException('Expecting mandatory parameters!');
        }

        $book = $this->bookRepository->findOneBy(['isbn' => $isbn]);
        if (!empty($book)){
            return new JsonResponse(['status' => 'Book Exist!'], Response::HTTP_CREATED);
        }


        $this->bookRepository->saveBook($isbn, $title, $subtitle, $author, $published, $publisher, $pages, $description, $website, $category, $imageUrl);

        return new JsonResponse(['status' => 'Book created!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/book/addmult", name="addmult_book", methods={"POST"})
     */
    public function addmult(Request $request): JsonResponse
    {
        $library = json_decode($request->getContent(), true);
        $books_created = 0;


        for($i = 0; $i < count($library['books']); ++$i) {

            $data = $library['books'][$i];

            $isbn = $data['isbn'];
            $title = $data['title'];
            $subtitle = $data['subtitle'];
            $author = $data['author'];
            $published = date_create($data['published']);
            $publisher = $data['publisher'];
            $pages = $data['pages'];
            $description = $data['description'];
            $website = $data['website'];
            $category = $data['category'];

            if (isset($data['imageUrl'])) {
               $imageUrl = $data['imageUrl']; 
            }
            else{
                $imageUrl = null;
            }
            
            if (empty($isbn) || empty($title) || empty($author) || empty($pages)) {
                throw new NotFoundHttpException('Expecting mandatory parameters!');
            }

            $book = $this->bookRepository->findOneBy(['isbn' => $isbn]);
            if (!empty($book)){
                continue;
            }

            $this->bookRepository->saveBook($isbn, $title, $subtitle, $author, $published, $publisher, $pages, $description, $website, $category, $imageUrl);
            $books_created+=1;

        }

        return new JsonResponse(['status' => $books_created . ' Books created!'], Response::HTTP_CREATED);
    }

    /**
     * @Route("/book/{isbn}", name="get_one_book", methods={"GET"})
     */
    public function get($isbn): JsonResponse
    {
        $book = $this->bookRepository->findOneBy(['isbn' => $isbn]);

        if (empty($book)){
            return new JsonResponse(array(), Response::HTTP_OK);
        }

        $data = [
            'isbn' => $book->getIsbn(),
            'title' => $book->getTitle(),
            'subtitle' => $book->getSubtitle(),
            'author' => $book->getAuthor(),
            'published' => $book->getPublished(),
            'publisher' => $book->getPublisher(),
            'pages' => $book->getPages(),
            'description' => $book->getDescription(),
            'website' => $book->getWebsite(),
            'category' => $book->getCategory(),
            'imageUrl' => $book->getImageUrl(),
        ];

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/book", name="get_all_books", methods={"GET"})
     */
    public function getAll(): JsonResponse
    {
        $books = $this->bookRepository->findAll();
        $data = [];

        if (empty($books)){
            return new JsonResponse(array(), Response::HTTP_OK);
        }

        foreach ($books as $book) {
            $data[] = [
                'isbn' => $book->getIsbn(),
                'title' => $book->getTitle(),
                'subtitle' => $book->getSubtitle(),
                'author' => $book->getAuthor(),
                'published' => $book->getPublished(),
                'publisher' => $book->getPublisher(),
                'pages' => $book->getPages(),
                'description' => $book->getDescription(),
                'website' => $book->getWebsite(),
                'category' => $book->getCategory(),
                'imageUrl' => $book->getImageUrl(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/book/category/{category}", name="get_all_books_category", methods={"GET"})
     */
    public function getAllCategory($category): JsonResponse
    {
        $books = $this->bookRepository->findBy(['category' => $category]);
        $data = [];

        if (empty($books)){
            return new JsonResponse(array(), Response::HTTP_OK);
        }

        foreach ($books as $book) {
            $data[] = [
                'isbn' => $book->getIsbn(),
                'title' => $book->getTitle(),
                'subtitle' => $book->getSubtitle(),
                'author' => $book->getAuthor(),
                'published' => $book->getPublished(),
                'publisher' => $book->getPublisher(),
                'pages' => $book->getPages(),
                'description' => $book->getDescription(),
                'website' => $book->getWebsite(),
                'category' => $book->getCategory(),
                'imageUrl' => $book->getImageUrl(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/book/publishedbefore/{year}", name="get_all_books_published_before", methods={"GET"})
     */
    public function getAllPublishedBefore($year): JsonResponse
    {
        $date_limit = date_create_from_format('!Y', $year);
        $books = $this->bookRepository->findPublishedBefore($date_limit);
        $data = [];

        if (empty($books)){
            return new JsonResponse(array(), Response::HTTP_OK);
        }

        foreach ($books as $book) {
            $data[] = [
                'isbn' => $book->getIsbn(),
                'title' => $book->getTitle(),
                'subtitle' => $book->getSubtitle(),
                'author' => $book->getAuthor(),
                'published' => $book->getPublished(),
                'publisher' => $book->getPublisher(),
                'pages' => $book->getPages(),
                'description' => $book->getDescription(),
                'website' => $book->getWebsite(),
                'category' => $book->getCategory(),
                'imageUrl' => $book->getImageUrl(),
            ];
        }

        return new JsonResponse($data, Response::HTTP_OK);
    }

    /**
     * @Route("/book/delete/{isbn}", name="delete_book", methods={"DELETE"})
     */
    public function delete($isbn): JsonResponse
    {
        $book = $this->bookRepository->findOneBy(['isbn' => $isbn]);

        if (empty($book)){
            return new JsonResponse(['status' => 'Book Not Exist!'], Response::HTTP_OK);
        }

        $this->bookRepository->removeBook($book);

        return new JsonResponse(['status' => 'Book deleted'], Response::HTTP_NO_CONTENT);
    }

    /**
     * @Route("/book", name="book")
     */
    /*public function index(): Response
    {
        return $this->json([
            'message' => 'Welcome to your new controller!',
            'path' => 'src/Controller/BookController.php',
        ]);
    }*/
}
