<?php

//API URL
$url = 'http://api.agual.es/book/add';

//create a new cURL resource
$ch = curl_init($url);

//setup request to send json via POST
$data = array(
      "isbn" => "9781593275846",
      "title" => "Eloquent JavaScript, Second Edition",
      "subtitle" => "A Modern Introduction to Programming",
      "author" => "Marijn Haverbeke",
      "published" => "2014-12-14T00:00:00.000Z",
      "publisher" => "No Starch Press",
      "pages" => 472,
      "description" => "JavaScript lies at the heart of almost every modern web application, from social apps to the newest browser-based games. Though simple for beginners to pick up and play with, JavaScript is a flexible, complex language that you can use to build full-scale applications.",
      "website" => "http://eloquentjavascript.net/",
      "category" => "Classic",
      "imageUrl" => array('https://cdn.icreatia.es/media/mageplaza/blog/post/uploads/Logos-baja.jpg','https://static.mercadonegro.pe/wp-content/uploads/2019/11/22193228/7-tipos-de-logos-que-no-debes-pasar-por-alto.jpg','https://promocionmusical.es/wp-content/uploads/2019/05/logo-empresas.jpg')
);
$payload = json_encode($data);

//attach encoded JSON string to the POST fields
curl_setopt($ch, CURLOPT_POSTFIELDS, $payload);

//set the content type to application/json
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));

//return response instead of outputting
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

//execute the POST request
$result = curl_exec($ch);

//close cURL resource
curl_close($ch);

var_dump($result);
?>
